use criterion::{black_box, criterion_group, criterion_main, Criterion};

use math_vec_static::StaticVec;

fn dot(c: &mut Criterion) {
    let mut group = c.benchmark_group("dot");
    group.bench_function("len_3", |b| {
        let vec = StaticVec {v: black_box([0.0, 1.1, 2.2]),};
        let v2 = StaticVec {v: black_box([3.3, 4.1, 2.5]),};
        b.iter(|| {
            vec.dot(&v2)
        })
    });
}

criterion_group!{
    name = benches;
    config = Criterion::default().sample_size(500);
    targets = dot
}
criterion_main!(benches);
