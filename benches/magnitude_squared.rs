use criterion::{black_box, criterion_group, criterion_main, Criterion};

use math_vec_static::StaticVec;

fn magnitude_squared(c: &mut Criterion) {
    let mut group = c.benchmark_group("magnitude_squared");
    group.bench_function("len_3", |b| {
        let vec = StaticVec {v: black_box([0.0, 1.1, 2.2]),};
        b.iter(|| {
            vec.magnitude_squared()
        })
    });
    group.bench_function("len_4", |b| {
        let vec = StaticVec {v: black_box([0.0, 1.1, 2.2, 4.4]),};
        b.iter(|| {
            vec.magnitude_squared()
        })
    });

    let mut arr: [f32; 64] = [0.0; 64];
    for i in 0..64 {
        arr[i] = (i as f32) * 1.2;
    }
}

fn magnitude_squared_len64(c: &mut Criterion) {
    let mut group = c.benchmark_group("magnitude_squared");
    //let arr: [f32; 64] = (0..64).map(|x| (x as f32) * 1.2).collect();
    let mut arr: [f32; 64] = [0.0; 64];
    for i in 0..64 {
        arr[i] = (i as f32) * 1.2;
    }

    group.bench_function("len_64", |b| {
        let vec = StaticVec {v: black_box(arr),};
        b.iter(|| {
            vec.magnitude_squared()
        })
    });
}

criterion_group!{
    name = benches;
    config = Criterion::default().sample_size(500);
    targets = magnitude_squared
}
criterion_main!(benches);
