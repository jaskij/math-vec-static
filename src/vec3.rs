use num_traits::{NumOps};

use crate::static_vector::StaticVec;

pub type Vec3<T> = StaticVec<T, 3>;

impl<T> Vec3<T> {
    pub fn new(x: T, y: T, z: T) -> Vec3<T> {
        return Vec3 { v: [x, y, z] };
    }

    pub fn set_x(&mut self, v: T) {
        self.v[0] = v;
    }

    pub fn set_y(&mut self, v: T) {
        self.v[1] = v;
    }

    pub fn set_z(&mut self, v: T) {
        self.v[2] = v;
    }
}

impl<T: Copy> Vec3<T> {
    pub fn x(&self) -> T {
        return self.v[0];
    }

    pub fn y(&self) -> T {
        return self.v[1];
    }

    pub fn z(&self) -> T {
        return self.v[2];
    }
}

impl<T: NumOps + Copy> Vec3<T> {
    pub fn cross(&self, other: &Vec3<T>) -> Vec3<T> {
        return Vec3 {
            v: [
                self[1] * other[2] - self[2] * other[1],
                self[2] * other[0] - self[0] * other[2],
                self[0] * other[1] - self[1] * other[0],
            ],
        };
    }

}
