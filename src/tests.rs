use test;

use crate::static_vector::StaticVec;
use crate::vec3::Vec3;

type VecUnsigned2 = StaticVec<u32, 2>;

#[test]
fn vec_add_assign() {
    let mut v = VecUnsigned2 {v: [0, 5]};
    v += VecUnsigned2{v: [4, 2]};
    assert_eq!(v, VecUnsigned2{v: [4, 7]});
}

#[test]
fn vec_sub_assign() {
    let mut v = VecUnsigned2 {v: [10, 20]};
    v -= VecUnsigned2{v: [4, 2]};
    assert_eq!(v, VecUnsigned2{v: [6, 18]});
}

#[test]
fn vec_add_assign_scalar() {
    let mut v = VecUnsigned2{v: [0, 5]};
    v += 3;
    assert_eq!(v, VecUnsigned2{v: [3, 8]});
}

#[test]
fn vec_sub_assign_scalar() {
    let mut v = VecUnsigned2{v: [10, 5]};
    v -= 3;
    assert_eq!(v, VecUnsigned2{v: [7, 2]});
}

#[test]
fn vec_mul_assign_scalar() {
    let mut v = VecUnsigned2{v: [0, 5]};
    v *= 3;
    assert_eq!(v, VecUnsigned2{v: [0, 15]});
}

#[test]
fn vec_div_assign_scalar() {
    let mut v = VecUnsigned2{v: [0, 9]};
    v /= 3;
    assert_eq!(v, VecUnsigned2{v: [0, 3]});
}

#[test]
fn vec_index() {
    let v = VecUnsigned2{v: [0, 5]};
    assert_eq!(5, v[1]);
}

#[test]
fn vec_mut_index() {
    let mut v = VecUnsigned2{v: [0, 5]};
    v[0] = 3;
    assert_eq!(v, VecUnsigned2{v: [3,5]});
}

#[test]
fn vec3_access() {
    let v = Vec3{v: [0, 1, 2]};
    assert_eq!(v.x(), 0);
    assert_eq!(v.y(), 1);
    assert_eq!(v.z(), 2);
}

#[test]
fn vec3_assign() {
    let mut v = Vec3{v: [0, 1, 2]};
    v.set_x(5);
    v.set_y(6);
    v.set_z(7);
    assert_eq!(v, Vec3{v: [5,6,7]})
}
