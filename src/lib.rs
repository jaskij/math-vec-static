mod static_vector;
mod vec3;
pub use static_vector::StaticVec;
pub use vec3::Vec3;

#[cfg(test)]
mod tests;
