use std::array::TryFromSliceError;
use std::convert::{From, TryFrom, TryInto};
use std::ops::{Add, AddAssign, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Sub, SubAssign};

use num_traits::{Float, NumOps, Zero};

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct StaticVec<T, const N: usize> {
    pub v: [T; N],
}

impl<T: Zero + NumOps + Copy + Mul + Add, const N: usize> StaticVec<T, N> {
    const LENGTH: usize = N;

    pub fn magnitude_squared(&self) -> T {
        let mut ret: T = T::zero();
        for i in 0..Self::LENGTH {
            ret = ret + self.v[i] * self.v[i]
        }
        return ret;
    }

    pub fn magnitude_squared_map(&self) -> T {
        v.into_iter().map(|x| x * x).sum()
    }

    pub fn magnitude_squared_fold(&self) -> T {
        v.into_iter().fold(T::zero(), |sum, x| sum = sum + x * x)
    }


    pub fn dot(&self, other: &Self) -> T {
        let mut ret: T = T::zero();
        for i in 0..Self::LENGTH {
            ret = ret + self.v[i] * other.v[i]
        }
        return ret;
    }
}

impl<T: Float + NumOps + Copy, const N: usize> StaticVec<T, N> {
    pub fn magnitude(&self) -> T {
        return self.magnitude_squared().sqrt();
    }
}

impl<T: Copy, const N: usize> TryFrom<&[T]> for StaticVec<T, N> {
    type Error = TryFromSliceError;

    fn try_from(value: &[T]) -> Result<Self, Self::Error> {
        let tmp: [T; N] = value.try_into()?;
        Ok(Self { v: tmp })
    }
}

impl<T: Copy, const N: usize> From<&[T; N]> for StaticVec<T, N> {
    fn from(value: &[T; N]) -> Self {
        Self { v: *value }
    }
}

impl<T, const N: usize> From<[T; N]> for StaticVec<T, N> {
    fn from(value: [T; N]) -> Self {
        Self { v: value }
    }
}

impl<T, const N: usize> Index<usize> for StaticVec<T, N> {
    type Output = T;

    fn index(&self, idx: usize) -> &Self::Output {
        return &self.v[idx];
    }
}

impl<T, const N: usize> IndexMut<usize> for StaticVec<T, N> {
    fn index_mut(&mut self, idx: usize) -> &mut Self::Output {
        return &mut self.v[idx];
    }
}

impl<T, const N: usize> IntoIterator for StaticVec<T, N> {
    type Item = T;
    type IntoIter = std::array::IntoIter<T, N>;

    fn into_iter(self) -> Self::IntoIter {
        std::array::IntoIter::new(self.v)
    }
}

impl<T: NumOps + Copy, const N: usize> AddAssign<StaticVec<T, N>> for StaticVec<T, N> {
    fn add_assign(&mut self, rhs: StaticVec<T, N>) {
        for (i, v) in rhs.v.iter().enumerate() {
            self.v[i] = self.v[i] + *v;
        }
    }
}

impl<T: NumOps + Copy, const N: usize> Add<StaticVec<T, N>> for StaticVec<T, N> {
    type Output = Self;

    fn add(self, rhs: StaticVec<T, N>) -> Self::Output {
        let mut tmp = self.clone();
        tmp += rhs;
        tmp
    }
}

impl<T: NumOps + Copy, const N: usize> SubAssign<StaticVec<T, N>> for StaticVec<T, N> {
    fn sub_assign(&mut self, rhs: StaticVec<T, N>) {
        for (i, v) in rhs.v.iter().enumerate() {
            self.v[i] = self.v[i] - *v;
        }
    }
}

impl<T: NumOps + Copy, const N: usize> Sub<StaticVec<T, N>> for StaticVec<T, N> {
    type Output = Self;

    fn sub(self, rhs: StaticVec<T, N>) -> Self::Output {
        let mut tmp = self.clone();
        tmp -= rhs;
        tmp
    }
}

impl<T: NumOps + Copy, const N: usize> AddAssign<T> for StaticVec<T, N> {
    fn add_assign(&mut self, rhs: T) {
        for v in &mut self.v {
            *v = *v + rhs
        }
    }
}

impl<T: NumOps + Copy, const N: usize> Add<T> for StaticVec<T, N> {
    type Output = StaticVec<T, N>;

    fn add(self, rhs: T) -> Self::Output {
        let mut tmp = self.clone();
        tmp += rhs;
        tmp
    }
}

impl<T: NumOps + Copy, const N: usize> SubAssign<T> for StaticVec<T, N> {
    fn sub_assign(&mut self, rhs: T) {
        for v in &mut self.v {
            *v = *v - rhs
        }
    }
}

impl<T: NumOps + Copy, const N: usize> Sub<T> for StaticVec<T, N> {
    type Output = StaticVec<T, N>;

    fn sub(self, rhs: T) -> Self::Output {
        let mut tmp = self.clone();
        tmp -= rhs;
        tmp
    }
}

impl<T: NumOps + Copy, const N: usize> MulAssign<T> for StaticVec<T, N> {
    fn mul_assign(&mut self, rhs: T) {
        for v in &mut self.v {
            *v = *v * rhs
        }
    }
}

impl<T: NumOps + Copy, const N: usize> Mul<T> for StaticVec<T, N> {
    type Output = StaticVec<T, N>;

    fn mul(self, rhs: T) -> Self::Output {
        let mut tmp = self.clone();
        tmp *= rhs;
        tmp
    }
}

impl<T: NumOps + Copy, const N: usize> DivAssign<T> for StaticVec<T, N> {
    fn div_assign(&mut self, rhs: T) {
        for v in &mut self.v {
            *v = *v / rhs
        }
    }
}

impl<T: NumOps + Copy, const N: usize> Div<T> for StaticVec<T, N> {
    type Output = StaticVec<T, N>;

    fn div(self, rhs: T) -> Self::Output {
        let mut tmp = self.clone();
        tmp /= rhs;
        tmp
    }
}
